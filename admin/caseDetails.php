<?php require './../dbconfig.php';
$sql = "SELECT * FROM results WHERE test_id={$_GET['id']}";
$res = $mysqli->query($sql);
$row = $res->fetch_assoc();
$answers = [$row['a'],$row['b'],$row['c'],$row['d'],$row['e'],$row['f'],$row['g'],$row['h'],$row['i'],$row['j'],$row['k']];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Intelligent Covid Home Test & Data Analytics App</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta name="description" content="Dont take a chance, check for COVID-19 symptoms while staying at home. Take a small assesment. Protect yourself, your family and society from dangerous CORONAVIRUS">
<link rel="icon" href="images/favicon.png" type="image/png" sizes="16x16">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./styles/custom.css">
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<h3 class="text-center">Case Details</h3>
<div class="card">
<table class="table">
<tr><td>State</td><td><?php
$sqls = "SELECT state.* FROM locale state, locale district WHERE district.name = '{$row['district']}' AND district.parent = state.id";
$ress = $mysqli->query($sqls);
$rows = $ress->fetch_assoc();
echo $rows['name'];
?></td></tr>
<tr><td>District</td><td><?=$row['district']?></td></tr>
<tr><td>Geo Coordinates</td><td><?=$row['latitude']?>, <?=$row['longitude']?></td></tr>
<tr><td>IP Address</td><td><?=$row['ip']?></td></tr>
<tr><td>Date</td><td><?=$row['created']?></td></tr>
<tr><td colspan="2" class="text-center"><h3>SYMPTOM ANALYSIS</h3></tr>
<?php
$sqlq = "SELECT * FROM questions";
$resq = $mysqli->query($sqlq);
while ($rowq = $resq->fetch_assoc()) {
    ?>
    <tr><td><?=$rowq['question']?></td><td style="font-weight: bold;color:<?=$answers[$rowq['id']-1]=='yes'?'red':'green'?>"><?=$answers[$rowq['id']-1]?></td></tr>
    <?php
}
?>
<tr><td>RISK</td><td>
<?php
$score = $row['result'];
if ($score>=0&&$score<=3) { ?>
<td style="color:green">LOW</td>
<?php } else if ($score>3&&$score<=6) { ?>
<td style="color:orange">MEDIUM</td>
<?php } else { ?>
<td style="color:red">HIGH</td>
<?php }
?>
</td></tr>
</table>

</div>
</div>
</div>
<div class="row">
<div class="col-md-12 text-center">
<a href="./index.php" class="btn btn-danger">Go to main</a>
</div>
</div>
</div>
<div class="container-fluid" style="position:relative;bottom:0px">
<div class="row">
<div class="col-md-12 text-right" style='font-size: 11px'>
<hr>
Powered By: JSR Annamayya, J Vijay Punnarao, VESS Krishna Krovvidi
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js'></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js'></script>
</body>
</html>