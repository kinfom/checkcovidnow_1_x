<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Admin - Intelligent Covid Home Test & Data Analytics App</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

 <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
 </head>
<body>
<div class="container">
<div class="row" style="margin-top:20px">
<div class="col-md-12">
<h2 class="text-center">

Intelligent Covid Home Test & Data Analytics App - Results (<?=$_GET['district']?>)
</h2>
<hr>
</div>
</div>
<div class="row">
<div class="col-md-12">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item">
    <?php
    require './../dbconfig.php';
$sql = "SELECT state.name, state.id FROM locale state, locale district WHERE district.id={$_GET['id']} AND state.id=district.parent";
$res = $mysqli->query($sql);
$row = $res->fetch_assoc();
    ?>
    <a href="stateData.php?id=<?=$row['id']?>&state=<?=urlencode($row['name'])?>"><?=$row['name']?>
    </a></li>
    <li class="breadcrumb-item active" aria-current="page"><?=$_GET['district']?></li>
  </ol>
</nav>
</div>
</div>
<div class="row">
<div class="col-md-12">
<table class="table table-primary table-hover" id="myTable">
<thead>
<tr>
<th>ID</th>
<th>Score</th>
<th>Risk</th>
<th>Latitude</th>
<th>Longitude</th>
<th>IP Address</th>
<th>State</th>
<th>District</th>
<th>Date</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<?php
require '../dbconfig.php';
$sql = "SELECT * FROM `results` WHERE district = '{$_GET['district']}'";
$result = $mysqli->query($sql);
while($row=$result->fetch_assoc()) { ?>
<tr>
<td><?=$row['test_id']?></td>
<td><?=$row['result']?></td>
<?php
$score = $row['result'];
if ($score>=0&&$score<=3) { ?>
<td style="color:green">LOW</td>
<?php } else if ($score>3&&$score<=6) { ?>
<td style="color:orange">MEDIUM</td>
<?php } else { ?>
<td style="color:red">HIGH</td>
<?php }
?>
<td><?=$row['latitude']?></td>
<td><?=$row['longitude']?></td>
<td><?=$row['ip']?></td>
<td>
<?php
$sqls = "SELECT state.* FROM locale state, locale district WHERE district.name = '{$row['district']}' AND district.parent = state.id";
$ress = $mysqli->query($sqls);
$rows = $ress->fetch_assoc();
echo $rows['name'];
?>
</td>
<td><?=$row['district']?></td>
<td><?=$row['created']?></td>
<td><a href="caseDetails.php?id=<?=$row['test_id']?>" title="Case Details" class="btn btn-success"><i class="fa fa-eye"></i></a></td>
</tr>
<?php }
?>

</tbody>
</table>

</div>
</div>
</div>
<div class="container-fluid" style="position:relative;bottom:0px">
<div class="row">
<div class="col-md-12 text-right" style='font-size: 11px'>
<hr>
Powered By: JSR Annamayya, J Vijay Punnarao, VESS Krishna Krovvidi
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js'></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src='https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js'></script>

    <script>
    $(document).ready( function () {
    $('#myTable').DataTable( {
        "order": [[ 0, "desc" ]],
                dom: 'Bfrtip',

        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );

    </script>
</body>
</html>