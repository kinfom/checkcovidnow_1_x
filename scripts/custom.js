keys = [];
alert(
  'Inorder to complete the test successfully, please select "Allow" in the following prompt if it occurs.'
);
function getLocation() {
  localStorage.clear();
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(savePosition);
  } else {
    console.log("Geolocation is not supported by this browser.");
  }
}

function savePosition(position) {
  // keys[0] = position.coords.latitude;
  // keys[1] = position.coords.longitude;
  lat = position.coords.latitude;
  lng = position.coords.longitude;
  keys.push(lat);
  keys.push(lng);
  console.log(
    "test",
    keys,
    position.coords.latitude,
    position.coords.longitude
  );
}

getLocation();

$("#active").on("click", "#nextbtn", function () {
  console.log("form submitted..");
  let qid = $("#nextbtn").data("id");

  if (qid == 0) {
    if (keys.length == 0) {
      alert(
        "Please allow application to access your locations!\n\nNote: If you are not able to allow location access, please open https://www.checkcovidnow.com in an incognito (Chrome) or private window (Firefox)"
      );
      return false;
      location.reload();
    }
    answer = $("#question").val();
    state = $("#state").val();
    console.log("state", state);
    if (!state) {
      alert("Please select your state.");
      return false;
    }
    if (answer == "") {
      alert("Please select your district.");
      return false;
    }
  } else {
    answer = $("input[name='question']:checked").val();
    if (answer == "") {
      alert("Please select yes/no.");
      return false;
    }
  }
  if (typeof answer == "undefined") {
    alert("Please select Yes or No.");
    return false;
  }
  console.log(qid, answer);
  keys.push(answer);
  console.log(keys);
  // ajax call for next question
  $.ajax({
    url: "../getQuestion.php",
    data: { id: qid, keys: keys },
    success: function (data) {
      console.log(data);
      $("#active").html(data);
    },
  });
});
$("#state").change(function () {
  console.log("state selected");
  let id = $(this).val();
  $.ajax({
    url: "../getDistricts.php",
    data: { id: id },
    success: function (data) {
      $("#question").html(data);
    },
  });
});
