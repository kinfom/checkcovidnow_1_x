<?php require './dbconfig.php'?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Intelligent Covid Home Test & Data Analytics App</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta name="description"
        content="Dont take a chance, check for COVID-19 symptoms while staying at home. Take a small assesment. Protect yourself, your family and society from dangerous CORONAVIRUS">
    <link rel="icon" href="images/favicon.png" type="image/png" sizes="16x16">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
     
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./styles/custom.css">
    <style>

        table > tbody > tr > td:first-child {
            font-weight: bold;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 offset-md-4" id='app-container'>
                <div class="row">
                    <div class="col-md-12" id="header">
                        <h5>
                            Intelligent Covid Home Test & Data Analytics App

                        </h5>

                    </div>
                </div>
               <div class="row">
                <div class="col-12" style="height:420px;overflow:scroll">
                    <table class='table table-responsive table-hover table-success' style='height:inherit; overflow: scroll;width: inherit'>
                        <thead>
                            <tr>
                                <th>District</th>
                                <th>Helpline</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $sql = "SELECT * FROM locale WHERE parent=0 ORDER BY name ASC";
                            $res = $mysqli->query($sql);
                            while ($row = $res->fetch_assoc()){
                                ?>
                                <tr>
                                    <td><?=$row["name"]?></td>
                                    <td><a href="tel:<?=$row["helpline"]?>"><?=$row["helpline"]?></a></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
               </div>
                <hr>
                <div class="row">
                    <div class="col-12 text-center h6">
                        <span class="counter h3">
                            <?php
        $sql = 'SELECT COUNT(*) AS count FROM results';
        $res = $mysqli->query($sql);
        $row = $res->fetch_assoc();
        echo $row['count'];
        ?>
                        </span>+ and still counting... <br>responsible citizens got tested here to ensure they are safe.
                        <br>IT'S YOUR TURN NOW..!
                    </div>
                </div>
                <div class="row" style="position: absolute; bottom: 0px;width:100%;background:#41b3a3">
                    <div class="col-12 text-center" style="background:#41b3a3">

                        <div class='' style="font-size:12px;color:yellow;width:100%;padding:10px;">Powered by: JSR
                            Annamayya, J Vijay Punnarao,<br>VESS Krishna Krovvidi <br><span style="font-weight:bold;">National Youth Awardees Federation of India®</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- <script src="./scripts/custom.js"></script> -->
    <script>
        $('.counter').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 5000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>
</body>

</html>