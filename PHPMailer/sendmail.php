<?php
require_once 'SMTPMailer.php';

$message = "";
$to = 'krishna.kkarthi@gmail.com';
$subject = 'CheckCovidNow - High Risk Case Identified';
$message .= "Dear User, <br>";
$message .= "<b>Please find the details of the case below which is predicted to be of HIGH risk for COVID-19 based on the AI self test of symptoms and conditions on our website https://www.checkcovidnow.com. <br> Thank you for your time.";
$message .= "<p>Regards, <br> CheckCovidNow Team.</p>";

echo $subject;
SMTPMailer($to, $subject, $message);
