<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sending Mail</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" id="loading">
                <img src="../../assets/img/loading.jpg" alt="">
                <p class="text-center">Sending Mail..</p>
            </div>
            <div class="col-md-12 text-center" id="mailsent">
                <i class="fa fa-check fa-5x text-success"></i>
                <!-- <img src="../../assets/img/loading.jpg" alt="" > -->
                <p class="text-center">Mail Sent Successfully <br>
                    Redirecting Back..
                </p>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#loading').show();
            $('#mailsent').hide();

            $.ajax({
                url: 'sendmail.php',
                success: function(data) {
                    $('#loading').hide();
                    $('#mailsent').show();
                    setTimeout(() => {
                        window.close('', '_parent', '');
                    }, 2000)
                }
            })
        })
    </script>
</body>

</html>