<?php require './dbconfig.php'?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Intelligent Covid Home Test & Data Analytics App</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&family=Roboto&display=swap" rel="stylesheet">
    <meta name="description" content="Dont take a chance, check for COVID-19 symptoms while staying at home. Take a small assesment. Protect yourself, your family and society from dangerous CORONAVIRUS">
<link rel="icon" href="images/favicon.png" type="image/png" sizes="16x16">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./styles/custom.css">
</head>
<body>
    <div class="container-fluid">
    <div class="row">
    <div class="col-md-4 offset-md-4" id='app-container'>
        <div class="row">
            <div class="col-md-12" id="header">
            <h5>
            Intelligent Covid Home Test & Data Analytics App
            
            </h5>
<div id="google_translate_element"></div>

            </div>
        </div>
        <div class="row">
        <div class="col-md-12">
        <blockquote class="blockquote text-center mt-1" style="font-size: 12px">
  <p class="mb-1">Your participation matters a lot. Please be genuine in your inputs and let's fight together against COVID-19. Remember, it matters many lives..</p>
  <footer class="blockquote-footer">Team  <cite title="Source Title">CheckCovidNow</cite></footer>
</blockquote>
        </div>
        </div>
        <div class="row" style="background: #e27d60; color: white;padding: 7px;font-weight: bold">
            <div class="col-md-12" style="font-size: 13px">
            <marquee behavior="" direction="">Wash your hands thoroughly with soap atleast for every 2 hours | Cough into your elbow | Don't touch your face | Maintain social distance | Stay Home | Stay Safe</marquee>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="main">
            <div id="active">
              <label for="state">Select your state</label>
              <select name="state" id="state" class="form-control">
              <option value="">Select</option>
              <?php
              $sqlss = "SELECT * FROM locale WHERE parent = 0 ORDER BY name ASC";
              $ress = $mysqli->query($sqlss);
              while($rows=$ress->fetch_assoc()) {
                  ?>
                  <option value="<?=$rows['id']?>"><?=$rows['name']?></option>
                  <?php
              }
              ?>
              </select>
              <br>
            <label for="district">Select your district</label>
            <select name="question" id="question" class="form-control" required>
            <option value=""></option>

            </select>
            <div id="button-area">
            <div class='row text-center'>
            <div class="col-12">
            
            <button id="nextbtn" data-id='0' class="btn btn-lg btn-success">Next</button>
            </div>
            </div>
            </div>
            </div>
            </div>
        </div>
        <hr>
        <div class="row">
        <div class="col-12 text-center h6">
        <span class="counter h4">
        <?php
        $sql = 'SELECT COUNT(*) AS count FROM results';
        $res = $mysqli->query($sql);
        $row = $res->fetch_assoc();
        echo $row['count'];
        ?>
        </span>+ got tested, and still counting... 
         <br>IT'S YOUR TURN NOW..!
        </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">

            <a class="btn btn-info"href="helplines.php">Helpline Contacts</a>
          </div>
        </div>
       <div class="row" style="position: absolute; bottom: 0px;width:100%;background:#41b3a3">
       <div class="col-12 text-center" style="background:#41b3a3">
       <div class='' style="font-size:12px;color:yellow;width:100%;padding:10px;">Powered by: JSR Annamayya, J Vijay Punnarao,<br>VESS Krishna Krovvidi <br><span style="font-weight:bold;">National Youth Awardees Federation of India®</span></div>
       </div>
       </div>
    </div>
    </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="./scripts/custom.js"></script>
<script>
$("#state").change(function() {
  console.log("state selected");
  let id = $(this).val();
  $.ajax({
    url: "../getDistricts.php",
    data: { id: id },
    success: function(data) {
      $("#question").html(data);
    }
  });
});
$('.counter').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
</script>

<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>
</html>