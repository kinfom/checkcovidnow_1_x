<?php
require './dbconfig.php';
$nextid = $_GET['id']+1;
// echo $nextid;

if ($nextid>11) {
    $keys = $_GET['keys'];
    // var_dump($keys);
    $lat = $keys[0]?$keys[0]:0;
    $long = $keys[1]?$keys[1]:0;
    $dist = $keys[2];
    $yes = 0;
    $no = 0;
    $ip = getenv('HTTP_CLIENT_IP')?:
getenv('HTTP_X_FORWARDED_FOR')?:
getenv('HTTP_X_FORWARDED')?:
getenv('HTTP_FORWARDED_FOR')?:
getenv('HTTP_FORWARDED')?:
getenv('REMOTE_ADDR');
    for($i=3;$i<=13;$i++) {
        if ($keys[$i]=='yes') {
            $yes = $yes + 1;
        } else {
            $no = $no + 1;
        }
    }
    if ($yes>=0&&$yes<=3) {
        $color = 'green';
        $risk = 'LOW';
        $msg= 'You may be stressed, get some rest';
    } else if ($yes>3&&$yes<=6) {
          $color = 'orange';
        $risk = 'MEDIUM';
        $msg= 'Chances are medium, consult a doctor';
    } else {
         $color = 'red';
        $risk = 'HIGH';
        $msg= 'Chances are very high, get self isolated and treated';
    }
    $sql = "INSERT INTO `results` (`ip`,`district`, `latitude`, `longitude`, `a`, `b`, `c`, `d`, `e`, `f`, `g`, `h`, `i`, `j`, `k`, `result`, `created`, `modified`) VALUES ('$ip','$dist', '$lat', '$long', '{$keys[3]}', '{$keys[4]}', '{$keys[5]}', '{$keys[6]}', '{$keys[7]}', '{$keys[8]}', '{$keys[9]}', '{$keys[10]}', '{$keys[11]}', '{$keys[12]}', '{$keys[13]}', '$yes', '{$now}', NULL)";
    $res = $mysqli->query($sql);
    if ($res) {
      // get state from district
      $sqlst = "SELECT state.name AS state FROM locale state, locale district WHERE district.name = '{$dist}' AND district.parent = state.id";
      $resst = $mysqli->query($sqlst);
      $rowst = $resst->fetch_assoc();
      $state = $rowst["state"];
      
      // trigger mail for HIGH risk cases on selected states
      if (($risk=='HIGH')&&($state=='Andhra Pradesh')) {
      require './PHPMailer/SMTPMailer.php';
      

      $message = "";
$to = 'krishna.kkarthi@gmail.com';
$subject = 'CheckCovidNow - '.$state.' - High Risk Case Identified';
$message .= "Dear User, <br>";
$message .= "<b>Please find the details of the case below which is predicted to be of HIGH risk for COVID-19 based on the AI self test of symptoms and conditions on our website https://www.checkcovidnow.com. <br> .";

  $message .= "<br>State: ".$state;
  $message .= '<br>District: '.$dist;
  $message .= '<br>IP Address: '.$ip;
  $message .= '<br>Latitude: '.$lat;
  $message .= '<br>Longitude: '.$long;

$message .= "<br>Thank you for your time.<p>Regards, <br> CheckCovidNow Team.</p>";

// set to address based on state
switch ($state) {
  case 'Andhra Pradesh': 
    $to = ['cfwhyd@yahoo.com', 'prl.secy.hmfwap@gmail.com', 'mdnrhmap@gmail.com'];
  break;
}
SMTPMailer($to, $subject, $message);
}
      ?>
    <div class="card" style="padding: 10px">
    <h4 class="text-success">Test Result</h4>
    <p>
        Thank you for voluntarily taking this test!
    </p>
    <p>
     Your risk of having COVID-19 is <span style="color: <?=$color?>"><?=$risk?></span>
    </p>
    <p>
    <?=$msg?>
    </p>
    <p>
    <button onclick="location.reload()" class="btn btn-primary">Take another test</button>
    </p>
    <p>
    For any queries, complaints and feedback, please write to us at <a href="mailto:wesupport@checkcovidnow.com">wesupport@checkcovidnow.com</a>
    </p>
    
    </div>
      <!-- <div style="font-size:11px; margin-top:10px;width:100%">Powered by: JSR Annamayya, J Vijay Punnarao,<br>VESS Krishna Krovvidi</div> -->
    <?php
    } else {
        die($mysqli->error.$sql);
    }
} else {
$sql = "SELECT * FROM questions WHERE id={$nextid}";
$res = $mysqli->query($sql);
$row = $res->fetch_assoc();

?>
<div style="text-align: center;">

<img height="100px" src="images/<?=$row['image']?>" alt="">
<br>
<p><?=$row['question']?></p>

<div class="btn-group btn-group-toggle" data-toggle="buttons">
  <label class="btn btn-secondary btn-lg ">
    <input type="radio" name="question" value="yes" id="yes" autocomplete="off" > Yes
  </label>
  <label class="btn btn-secondary btn-lg">
    <input type="radio" name="question" value="no" id="no" autocomplete="off" > Not Present
  </label>
</div>
<div id="button-area">
            <button id="nextbtn" data-id='<?= $nextid?>' class="btn btn-lg btn-success">Next</button>
             <!-- <p style="font-size:11px;position: absolute; bottom:0px; margin-top:40px;width:100%">Powered by: JSR Annamayya, J Vijay Punnarao,<br>VESS Krishna Krovvidi</p> -->
</div>

</div>
<?php } ?>